package com.anton.demo.repositories;


import com.anton.demo.univer.Student;
import org.springframework.data.repository.CrudRepository;

public interface StudentRepository extends CrudRepository<Student, Integer> {
     Student findByNameAndSurname(String name, String surname);
}
